package com.github.siiman.sales.scrappertest;

import com.github.siiman.sales.scrapper.Product;
import com.github.siiman.sales.scrapper.Scrapper;
import com.github.siiman.sales.scrapper.Selectors;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ScrapperTest {
    private String testUrl = "https://siiman.github.io/sales-test-page";
    private Selectors testSelectors = new Selectors.Builder().forProductContainer(".productContainer")
                                                             .forName(".productName")
                                                             .forOldPrice(".productOldPrice")
                                                             .forNewPrice(".productNewPrice")
                                                             .forDiscount(".productDiscount")
                                                             .build();
    private Scrapper testScrapper;

    @Before
    public void init() {
        testScrapper = new Scrapper(testUrl, testSelectors, 0);
    }

    @Test
    public void constructProductListAndAssertThatFieldsMatchExpectedValues() throws IOException {
        Document page = testScrapper.pullHtmlPage(1);
        List<Product> testProducts = testScrapper.constructProductList(page);
        assertThat(testProducts.size(), is(1));
        assertThat(testProducts.get(0).getName(), is("Test Cheese"));
        assertThat(testProducts.get(0).getNewPrice(), is(9.5));
        assertThat(testProducts.get(0).getOldPrice(), is(12.6));
        assertThat(testProducts.get(0).getDiscount(), is("-20%"));
        assertThat(testScrapper.extractNumberOfPages(page), is(1));
    }
}