package com.github.siiman.sales;

import com.github.siiman.sales.scrapper.Scrapper;
import com.github.siiman.sales.scrapper.Selectors;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ScheduledScrapper {
    @Scheduled(cron = "0 12 * * * ?")
    public void runScrapperForRimi() throws IOException {
        String rimiURL = "https://www.rimi.ee/pakkumised";
        Selectors rimiSelectors = new Selectors.Builder().forProductContainer(".offer-card__content")
                                                         .forName(".offer-card__name")
                                                         .forOldPrice(".old")
                                                         .forNewPrice(".price-badge__left, .price-badge__right__top")
                                                         .forDiscount(":matchesOwn(%$)")
                                                         .build();
        Scrapper rimi = new Scrapper(rimiURL, rimiSelectors, 1);
        rimi.scrapePages();
    }

    @Scheduled(cron = "0 12 * * * ?")
    public void runScrapperForSelver() throws IOException {
        String selverURL = "https://www.selver.ee/soodushinnaga-tooted";
        Selectors selverSelectors = new Selectors.Builder().forProductContainer(".col-lg-3.col-md-3.col-sm-4.col-xs-6.item")
                                                           .forName(".product-forName")
                                                           .forOldPrice(".old-price > .price > :eq(0), .regular-price > span > .price, .regular-price > .price > :eq(0)")
                                                           .forNewPrice(".special-price > .price > :eq(0), .product-image__badge--6 > .product-image__badge--label")
                                                           .forNumberOfPages(".toolbar-bottom .pages .last")
                                                           .build();
        Scrapper selver = new Scrapper(selverURL, selverSelectors, 2);
        selver.scrapePages();
    }

    @Scheduled(cron = "0 12 * * * ?")
    public void runScrapperForMaxima() throws IOException {
        String maximaURL = "https://www.maxima.ee/pakkumised";
        Selectors maximaSelectors = new Selectors.Builder().forProductContainer(".col-third")
                                                           .forName(".text")
                                                           .forOldPrice(".t2 .value")
                                                           .forNewPrice(".t1 .value, .t1 .cents")
                                                           .forDiscount(".percents")
                                                           .build();
        Scrapper maxima = new Scrapper(maximaURL, maximaSelectors, 4);
        maxima.scrapePages();
    }
}