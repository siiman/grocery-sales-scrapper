package com.github.siiman.sales.scrapper;

import java.util.Optional;

public class Selectors {
    private String forProductContainer;
    private String forName;
    private String forOldPrice;
    private String forNewPrice;
    private String forDiscount;
    private String forNumberOfPages;

    
    public Selectors(Builder builder) {
        this.forProductContainer = builder.forProductContainer;
        this.forName = builder.forName;
        this.forOldPrice = builder.forOldPrice;
        this.forNewPrice = builder.forNewPrice;
        this.forDiscount = builder.forDiscount;
        this.forNumberOfPages = builder.forNumberOfPages;
    }
    
    public static class Builder {
        private String forProductContainer;
        private String forName;
        private String forOldPrice;
        private String forNewPrice;
        private String forDiscount;
        private String forNumberOfPages;
        
        public Builder forProductContainer(String forProductContainer) {
            this.forProductContainer = forProductContainer;
            return this;
        }

        public Builder forName(String forName) {
            this.forName = forName;
            return this;
        }

        public Builder forOldPrice(String forOldPrice) {
            this.forOldPrice = forOldPrice;
            return this;
        }

        public Builder forNewPrice(String forNewPrice) {
            this.forNewPrice = forNewPrice;
            return this;
        }

        public Builder forDiscount(String forDiscount) {
            this.forDiscount = forDiscount;
            return this;
        }

        public Builder forNumberOfPages(String forNumberOfPages) {
            this.forNumberOfPages = forNumberOfPages;
            return this;
        }

        public Selectors build() {
            return new Selectors(this);
        }
    }

    public String getForProductContainer() {
        return this.forProductContainer;
    }

    public String getForName() {
        return this.forName;
    }

    public String getForOldPrice() {
        return this.forOldPrice;
    }

    public String getForNewPrice() {
        return this.forNewPrice;
    }

    public Optional<String> getForDiscount() {
        return Optional.ofNullable(this.forDiscount);
    }

    public Optional<String> getForNumberOfPages() {
        return Optional.ofNullable(this.forNumberOfPages);
    }
}