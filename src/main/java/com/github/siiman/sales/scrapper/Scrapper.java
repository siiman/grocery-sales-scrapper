package com.github.siiman.sales.scrapper;

import com.github.siiman.sales.dao.salesDAO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Scrapper {
    private static final Logger log = LoggerFactory.getLogger(Scrapper.class);
    private final String url;
    private final Selectors selectors;
    private final int storeId;

    public Scrapper(String url, Selectors selectors, int storeId) {
        this.url = url;
        this.selectors = selectors;
        this.storeId = storeId;
    }

    public Document pullHtmlPage(int pageNumber) throws IOException {
        String url = this.url;
        if (pageNumber > 1) {
            url = url + "?p=" + pageNumber;
        }
        log.info("Pulling HTML from " + url);
        return Jsoup.connect(url).get();
    }

    public int extractNumberOfPages(Document htmlPage) {
        return selectors.getForNumberOfPages()
                        .map(selector -> {
                            Elements htmlElement = htmlPage.select(selector);
                            return NumberUtils.toInt(htmlElement.text());
                        })
                        .orElse(1);
    }

    private String extractProductName(Element container) {
        Elements htmlElement = container.select(selectors.getForName());
        return htmlElement.text();
    }

    private double extractProductPrice(Element container, String selector) {
        Elements htmlElement = container.select(selector);
        return NumberUtils.toDouble(htmlElement.text()
                                               .replaceAll(" €", "")
                                               .replaceAll("[\\s,]", "."), -1);
    }

    private String extractProductDiscount(Element container, double oldPrice, double newPrice) {
        return selectors.getForDiscount()
                        .map(selector -> {
                            Elements htmlElement = container.select(selector);
                            return StringUtils.defaultIfEmpty(htmlElement.text(),
                                                              Product.calculateDiscount(oldPrice, newPrice));
                        })
                        .orElse(Product.calculateDiscount(oldPrice, newPrice));
    }

    public List<Product> constructProductList(Document htmlPage) {
        List<Product> products = new ArrayList<>();
        Elements productContainers = htmlPage.select(selectors.getForProductContainer());
        for (Element container : productContainers) {
            String name = extractProductName(container);
            double newPrice = extractProductPrice(container, selectors.getForNewPrice());
            double oldPrice = extractProductPrice(container, selectors.getForOldPrice());
            String discount = extractProductDiscount(container, oldPrice, newPrice);
            products.add(new Product.Builder().name(name)
                                              .newPrice(newPrice)
                                              .oldPrice(oldPrice)
                                              .discount(discount)
                                              .storeId(storeId)
                                              .build());
        }
        log.info("Constructed product list successfully!");
        return products;
    }

    public void scrapePages() throws IOException {
        salesDAO.setEndDateToProducts(storeId);
        for (int currentPage = 1, numberOfPages = 1; currentPage <= numberOfPages; currentPage++) {
            Document page = pullHtmlPage(currentPage);
            if (currentPage < 2) {
                numberOfPages = extractNumberOfPages(page);
            }
            List<Product> products = constructProductList(page);
            salesDAO.addProducts(products);
        }
    }
}