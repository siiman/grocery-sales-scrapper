package com.github.siiman.sales.scrapper;

public class Product {
    private String name;
    private double newPrice;
    private double oldPrice;
    private String discount;
    private int storeId;

    private Product(Builder builder) {
        this.name = builder.name;
        this.newPrice = builder.newPrice;
        this.oldPrice = builder.oldPrice;
        this.discount = builder.discount;
        this.storeId = builder.storeId;
    }

    public static class Builder {
        private String name;
        private double newPrice;
        private double oldPrice;
        private String discount;
        private int storeId;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder newPrice(double newPrice) {
             this.newPrice = newPrice;
            return this;
        }

        public Builder oldPrice(double oldPrice) {
             this.oldPrice = oldPrice;
            return this;
        }

        public Builder discount(String discount) {
             this.discount = discount;
            return this;
        }

        public Builder storeId(int storeId) {
             this.storeId = storeId;
            return this;
        }

        public Product build() {
            return new Product(this);
        }
    }

    public String getName() {
        return this.name;
    }

    public double getNewPrice() {
        return this.newPrice;
    }

    public double getOldPrice() {
        return this.oldPrice;
    }

    public int getStoreId() {
        return this.storeId;
    }

    public String getDiscount() {
        return this.discount;
    }

    public static String calculateDiscount(double oldPrice, double newPrice) {
        if (oldPrice < 0 || newPrice < 0) {
            return "N/A";
        }
        int discountPercent = (int) (newPrice * 100 / oldPrice);
        return String.format("-%d%%", 100 - discountPercent);
    }
}
