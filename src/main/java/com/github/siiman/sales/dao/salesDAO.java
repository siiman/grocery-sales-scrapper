package com.github.siiman.sales.dao;

import com.github.siiman.sales.scrapper.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class salesDAO {
    private static final String DB_CONNECTION = ""/*placeholder*/;
    private static final String DB_USER = ""/*placeholder*/;
    private static final String DB_PASSWORD = ""/*placeholder*/;
    private static java.util.Date currentDate = new java.util.Date();
    private static java.sql.Date DateInSql = new java.sql.Date(currentDate.getTime());
    private static final Logger log = LoggerFactory.getLogger(salesDAO.class);

    public static void addProducts(List<Product> inputProducts) {
        try (Connection connection = connectToDatabase();
             PreparedStatement productsToAdd = constructProductStatement(connection, inputProducts)) {
            productsToAdd.executeBatch();
            log.info("Inserted into DB");
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }

    public static void setEndDateToProducts(int storeId) {
        try (Connection connection = connectToDatabase();
             PreparedStatement endDateProducts = constructEndDateStatement(connection, storeId)) {
            endDateProducts.executeUpdate();
            log.info("Set end date to products of store " + storeId);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }

    private static Connection connectToDatabase() throws SQLException {
        return DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
    }

    private static PreparedStatement constructProductStatement(Connection connection, List<Product> inputProducts) throws SQLException {
        String statement = "INSERT INTO SALES.PRODUCT(PRODUCT_NAME, NEW_PRICE, OLD_PRICE, STORE_ID, DISCOUNT, START_DATE) " + "VALUES(?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE END_DATE = NULL";
        PreparedStatement productsToAdd = connection.prepareStatement(statement);
        for (Product product : inputProducts) {
            productsToAdd.setString(1, product.getName());
            productsToAdd.setDouble(2, product.getNewPrice());
            productsToAdd.setDouble(3, product.getOldPrice());
            productsToAdd.setInt(4, product.getStoreId());
            productsToAdd.setString(5, product.getDiscount());
            productsToAdd.setDate(6, DateInSql);
            productsToAdd.addBatch();
        }
        return productsToAdd;
    }

    private static PreparedStatement constructEndDateStatement(Connection connection, int storeId) throws SQLException {
        String statementString = "UPDATE SALES.PRODUCT SET END_DATE = ? WHERE STORE_ID = ? AND END_DATE IS NULL";
        PreparedStatement endDateProducts = connection.prepareStatement(statementString);
        endDateProducts.setDate(1, DateInSql);
        endDateProducts.setInt(2, storeId);
        return endDateProducts;
    }
}
