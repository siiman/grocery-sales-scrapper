A scrapper made to learn/practice Java 8. Granted, not the most innovative idea but sometimes you have to re-invent the wheel to find your way.

Runs a scheduled task on Spring Boot (https://spring.io/projects/spring-boot). The schedule, URL and HTML selectors for each store can be found in the `ScheduledScrapper.java` class. To execute this project just run `Application.java` after you've pulled and set up the project.

When run the scheduler executes the method `Scrapper.scrapePages()`. That itself is composed of many methods that 
1. set an end date to the previous database entries of a shop, 
2. pull the HTML files from the URL-s, 
3. select the products and their properties, 
4. update the database (if a product with the same name, old price and new price was in the database before for a given shop, the end date is set to null; if the product was not matched in the database, it's inserted). 

The product is stored using a helper class `Product` and the HTML selectors using `Selectors`.

The project uses JSoup (https://jsoup.org/) to pull the HTML file and extract the names, prices and discounts of products from it.

I used MySQL as my database management system. When you want to run this locally you should replace the `DB_CONNECTION`, `DB_USER` and `DB_PASSWORD` placeholders in the file. Also you'd need a schema named `sales` with tables like these:
```
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `new_price` double DEFAULT NULL,
  `old_price` double DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `discount` varchar(100) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `product_discount` (`product_name`,`new_price`,`old_price`,`store_id`,`discount`),
  KEY `store_id` (`store_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=259100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

Unit tests use JUnit 4. The mock page https://github.com/siiman/sales-test-page is used for the unit tests.

At the moment the project is tailored after Maxima's, Rimi's and Selver's webpages but could possibly be adapted to use with other shops as well.
